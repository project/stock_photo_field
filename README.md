# stock_photo_field

Provides a field type for downloading images from 3rd party stock photography 
providers.
